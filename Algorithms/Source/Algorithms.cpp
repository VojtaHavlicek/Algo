/**
A package of algorithms implemented for learning purposes.
Vojtech Havlicek

KOX apps 2012
*/

#include <iostream>
#include "Algorithms.h"

/**
App entry point
*/
int main()
{
	// testovaci sequence
	char* sequence = new char[5];
	sequence[0] = 'd';
	sequence[1] = 'b';
	sequence[2] = 'a';
	sequence[3] = 'c';
	sequence[4] = 'e';

	// sorting algorithm
	HeapSort::sort(sequence,5);
	
	//tracing data
	for(int i = 0; i < 5; i++)
		std::cout << sequence[i];
	std::cout << '\n';

	// sorting algorithm
	InsertionSort::sort(sequence,5);
	
	//tracing data
	for(int i = 0; i < 5; i++)
		std::cout << sequence[i];
	std::cout << '\n';

	return 0;
}