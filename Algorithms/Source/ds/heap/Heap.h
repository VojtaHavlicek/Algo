#pragma once
class Heap
{
public:
	Heap(unsigned int);
	~Heap(void);

	/**
	methods:
	*/
	void add(char);
	void scan(); //testing method.
	char removeRoot();
private:
	unsigned int initialSize;
	unsigned int lastIndex;
	char*        byteArray; //a pointer to a byteArray
};

