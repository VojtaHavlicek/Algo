#include "Heap.h"
#include <math.h>
#include <iostream>

/**
A simple implementation of binary heap.
Vojtech Havlicek

KOX apps 2012
---
Note: Heap property:
if B is child of A, then key A > key B
*/


Heap::Heap(unsigned int initialSize)
{
	this->initialSize = initialSize;
	byteArray         = new __declspec(nothrow) char[initialSize];
	
	if(byteArray == 0)
	{
		std::cout << "initialization failed\n";
		//return; 
	}

	lastIndex    = 0;
}


Heap::~Heap(void)
{
}


/**
adds an element to heap
*/
void Heap::add(char c) 
{
	if(byteArray == 0)
	{
		std::cout << "byteArray is null \n";
		return;
	}
	
	int tempIndex = lastIndex;

	//max, heapification
	for(;  tempIndex > 0 && *(byteArray+(tempIndex-1)/2) < c; tempIndex = (int) ((tempIndex - 1)/2)){
	
		*(byteArray+tempIndex) = *(byteArray+(tempIndex-1)/2);
	}

	*(byteArray+tempIndex) = c;
	lastIndex++;
}

/**
scans the heap, used for debugging
*/
void Heap::scan()
{
	for(unsigned int i = 0; i < lastIndex; i++){
		std::cout << byteArray[i];
	}
	std::cout << '\n';
}

/**
removes the root element
*/
char Heap::removeRoot()
{
	//saves the char to be prepared to return.
	char toReturn = *byteArray;
	
	//decreases the lastIndex value. This is needed in all cases
	if(lastIndex > 0)
		lastIndex--;
	else
		return '\0'; //no elements are in array. Therefore return NULL

	//if toReturn is the only element;
	if(lastIndex == 0)
		return toReturn; //protects unsigned index
	

	//changes root for last element
	char last  = *(byteArray+lastIndex);
	*byteArray = last;

	//downheap
	unsigned int index = 0; 

	//for each node in the structure, which in the skeleton of heap.
	for(;lastIndex >= index;) 
	{
		char left  = lastIndex >= (index*2) + 1 ? *(byteArray + (index*2) +1) : 0; // Peek at the left child node. 
		char right = lastIndex >= (index*2) + 2 ? *(byteArray + (index*2) +2) : 0; // + If out of index, take the lowest childnode possible.

		if(left >= right) //choose the bigger child node and swap it with current node
		{
			*(byteArray+index) = left;
			             index = (index*2) + 1;
		}else 
		{
			*(byteArray+index) = right;
			             index = (index*2) + 2;
		}
	}
	*(byteArray + (int)((index)/2)) = last; //replace the last peeked node with initially last node
	
	return toReturn; //return the initial root node
}
