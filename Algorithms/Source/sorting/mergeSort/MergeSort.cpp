#include "MergeSort.h"
#include <iostream>

/**
sorts the data character sequence using mergesort. 
the implementation originates from:
http://simplestcodings.blogspot.cz/2010/08/merge-sort-implementation-in-c.html
use 0 for low initially and length for high 
*/
 void MergeSort::sort(char* data, int low, int high)
{
	//char* dataSequence = new char[dataLength];
	int pivot;
	if(low < high)
	{
		pivot = (low+high)/2;          // set the pivot point in the middle of a new array
		sort(data, low,  pivot);       // sort left portion of subarray
		sort(data, pivot+1, high);     // sort right
		merge(data, low, pivot, high); // merge the subarrays !!!
	}

}

/**
merges the two sorted arrays. uses sentinels.
possibly ineficient usage of memory with b.
*/
void MergeSort::merge(char* data, int low, int pivot, int high)
{
	//this->data = data;

	int h,i,j,k;
	
	char b[50]; //is declared as a private member of the class
	h = low;	   // first element in 1st portion of subarray
	i = low;       
	j = pivot + 1; // first element in 2nd portion of subarray

	while((h <= pivot) && (j <= high))
	{
		if(data[h] <= data[j]) //if elem. in left portion is smaller than in left portion
		{
			b[i] = data[h]; // take the element to the final array
			h++;            // and increase the counter for the array
		}else
		{
			b[i] = data[j];
			j++;
		}
		i++;
	}

	// ---- if h > pivot, add all elements 
	if(h > pivot){
		for(k = j; k <= high; k++)
		{
			b[i] = data[k];
			i++;
		}
	}else{
		for(k = h; k <= pivot; k++)
		{
			b[i] = data[k];
			i++;
		}
	}

	for(k=low; k <= high; k ++) data[k] = b[k]; 
}

/**
scans the array
*/
void MergeSort::scan()
{
	/*std::cout<<'\n';
	for each (char c in b)
	{
		std::cout<<c;
	}*/
}