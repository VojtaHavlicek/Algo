#include "InsertionSort.h"
#include <iostream>

/**
insertion sort
Vojtech Havlicek
KOX apps 2012
*/

/**
Main procedure to run sorting on char array. 
Naive implementation.
*/
void InsertionSort::sort(char* arr, int length)
{
	int position     = 1;
	int tempPosition = position;

	/**
	includes safety counters
	*/
	for(position = 1; position < length/*(arr + position) != '\0'*/; position++){
		for(tempPosition = position; *(arr+tempPosition-1) > *(arr + tempPosition); tempPosition--)
		{
			//swap the two
			char temp = *(arr+tempPosition-1);
			*(arr+tempPosition-1) = *(arr + tempPosition);
			*(arr + tempPosition) = temp;
		}
	}
}

/**
A version of Insertion Sort with focus on 
minimum amount of operations done.
*/
void InsertionSort::optimizedSort(char* arr, int length)
{
	int position     = 1;
	int tempPosition = position;
	char temp;
	
	for(position = 1; position < length /**(arr + position) != '\0'*/; position++)
	{
		temp = *(arr + position);
		for(tempPosition = position; *(arr+tempPosition-1) > temp; tempPosition--)
		{
			//swap the two
			 *(arr + tempPosition) = *(arr+tempPosition-1);
		}
		*(arr + tempPosition) = temp;
	}
}
