#include "HeapSort.h"
#include <iostream>

/**
Implementation of an heapsort sorting algorithm

Vojtech Havlicek
KOX apps 2012
*/

void HeapSort::sort(char* data, int dataLength)
{
	Heap heap = Heap(dataLength);
	

	 int i = 0;
	for(;i < dataLength; i++)
	{
		heap.add(*(data + i));
	}
	
	heap.scan();
	
	for(i = dataLength-1; i >= 0; --i){
		*(data + i) = heap.removeRoot();
	}
}

void HeapSort::scan()
{
	/*for(int i=0; *(toReturn + i) != '\0' ;i++){
		std::cout<<*(toReturn + i);
	}
	std::cout << "\n";*/	
}


